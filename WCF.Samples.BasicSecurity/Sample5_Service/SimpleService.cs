﻿using SimpleService.Contracts;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Principal;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Sample5_Service
{
    public class SimpleService : ISimpleService
    {
        const string fileName = @"d:\ForDemo\SecretFile.txt";

        public string Method(string param1)
        {
            WindowsIdentity callerWindowsIdentity = ServiceSecurityContext.Current.WindowsIdentity;

            if (callerWindowsIdentity == null)
            {
                throw new InvalidOperationException();
            }

            using (callerWindowsIdentity.Impersonate())
            {
                return File.ReadAllText(fileName);
            }
        }
    }
}
