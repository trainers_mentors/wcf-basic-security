﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SimpleService.Contracts;
using System.ServiceModel;

namespace Sample4_Client
{
    [TestClass]
    public class UserPassword
    {
        [TestMethod]
        public void WindowsUser()
        {
            var channelFactory = new ChannelFactory<ISimpleService>("UserName");
            channelFactory.Credentials.UserName.UserName = "EPRUIZHW0060\\TestUser";
            channelFactory.Credentials.UserName.Password = "1qaz@WSX3edc$RFV";
            var channel = channelFactory.CreateChannel();

            Console.WriteLine(channel.Method("www"));
        }

        [TestMethod]
        public void NoWindowsUser()
        {
            var channelFactory = new ChannelFactory<ISimpleService>("UserName");
            channelFactory.Credentials.UserName.UserName = "user";
            channelFactory.Credentials.UserName.Password = "pass";
            var channel = channelFactory.CreateChannel();

            Console.WriteLine(channel.Method("www"));
        }


    }
}
