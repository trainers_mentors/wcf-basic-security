﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.ServiceModel;
using SimpleService.Contracts;

namespace Sample4_Client
{
    [TestClass]
    public class WindowsCredentials
    {
        [TestMethod]
        public void DefaultCredentials()
        {
            var channelFactory = new ChannelFactory<ISimpleService>("Windows");
            var channel = channelFactory.CreateChannel();

            Console.WriteLine(channel.Method("www"));
        }

        [TestMethod]
        public void CustomCredentials()
        {
            var channelFactory = new ChannelFactory<ISimpleService>("Windows");
            channelFactory.Credentials.Windows.ClientCredential =
                new System.Net.NetworkCredential("EPRUIZHW0060\\TestUser", "1qaz@WSX3edc$RFV");

            var channel = channelFactory.CreateChannel();

            Console.WriteLine(channel.Method("www"));
        }

    }
}
