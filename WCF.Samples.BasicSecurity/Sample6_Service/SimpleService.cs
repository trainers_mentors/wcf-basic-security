﻿using SimpleService.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Permissions;
using System.Text;
using System.Threading.Tasks;

namespace Sample6_Service
{
    public class SimpleService : ISimpleService
    {
        [PrincipalPermission(SecurityAction.Demand, Role="BUILTIN\\Administrators")]
        public string Method(string param1)
        {
            return "Result";
        }
    }
}
