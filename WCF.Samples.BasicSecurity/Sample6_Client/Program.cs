﻿using SimpleService.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Sample6_Client
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                var channelFactory = new ChannelFactory<ISimpleService>("Windows");
                var channel = channelFactory.CreateChannel();

                Console.WriteLine(channel.Method(""));
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            Console.ReadLine();
        }
    }
}
