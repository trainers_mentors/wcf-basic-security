﻿namespace Sample7_Client
{
    using System;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class CalcTests
    {
        [TestMethod]
        public void GetPI()
        {
            using (var client = new ServiceReference.CalculatorServiceClient())
            {
                client.ClientCredentials.UserName.UserName = "admin";
                client.ClientCredentials.UserName.Password = "admin";
                Console.WriteLine(client.PI());
            }
        }

        [TestMethod]
        public void TryAddSuccess()
        {
            using (var client = new ServiceReference.CalculatorServiceClient())
            {
                client.ClientCredentials.UserName.UserName = "user";
                client.ClientCredentials.UserName.Password = "user";
                Console.WriteLine(client.Add(1, 5));
            }
        }

        [TestMethod]
        public void TryAddFault()
        {
            using (var client = new ServiceReference.CalculatorServiceClient())
            {
                client.ClientCredentials.UserName.UserName = "test";
                client.ClientCredentials.UserName.Password = "test";
                Console.WriteLine(client.Add(1, 5));
            }
        }
    }
}
