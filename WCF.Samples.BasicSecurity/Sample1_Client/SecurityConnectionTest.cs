﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.ServiceModel.Description;
using System.ServiceModel;
using SimpleService.Contracts;

namespace Sample1_Client
{
    [TestClass]
    public class SecurityConnectionTest
    {
        Uri GetMachineNameAddress(Uri address)
        {
            var hostName = System.Net.Dns.GetHostName();
            var uriBuilder = new UriBuilder(address);

            uriBuilder.Host = hostName;
            return uriBuilder.Uri;
        }

        [TestMethod]
        public void ConnectWithMetadataReading()
        {
            var metadataAddress = "http://localhost:9090/Design_Time_Addresses/Sample1_Service/SimpleService/?wsdl";

            var metadataClient = new MetadataExchangeClient(new Uri(metadataAddress), MetadataExchangeClientMode.HttpGet);
            var wsdlImporter = new WsdlImporter(metadataClient.GetMetadata());
            var endpoint = wsdlImporter.ImportAllEndpoints()[0];

            var newAddress = new EndpointAddress(GetMachineNameAddress(endpoint.Address.Uri));
            var channelFactory = new ChannelFactory<ISimpleService>(endpoint.Binding, newAddress);

            var channel = channelFactory.CreateChannel();

            Console.WriteLine(channel.Method("Param1"));
        }

        [TestMethod]
        public void ConfigureSecurityInConfigFile()
        {            
            var channelFactory = new ChannelFactory<ISimpleService>("SimpleService_Configuration");
            var channel = channelFactory.CreateChannel();

            Console.WriteLine(channel.Method("Param1"));
        }

        [TestMethod]
        public void ConfigureSecurityInCode()
        {
            var binding = new WSHttpBinding();
            binding.Security.Mode = SecurityMode.Message;
            binding.Security.Message.ClientCredentialType = MessageCredentialType.Windows;

            var channelFactory = new ChannelFactory<ISimpleService>(binding, "http://localhost:9090/Design_Time_Addresses/Sample1_Service/SimpleService/");
            var channel = channelFactory.CreateChannel();

            Console.WriteLine(channel.Method("Param1"));
        }
    }
}
