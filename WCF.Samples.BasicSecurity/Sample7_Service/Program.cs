﻿namespace Sample7_Service
{
    using System;
    using System.ServiceModel;

    class Program
    {
        static void Main(string[] args)
        {
            using (var host = new ServiceHost(typeof(CalculatorService)))
            {
                // info: start application from admin in case of exception
                host.Open();
                Console.WriteLine("Host opened...");

                Console.ReadLine();
                host.Close();
            }
        }
    }
}
