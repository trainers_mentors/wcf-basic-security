﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UserAuthentication.cs" company="">
//   
// </copyright>
// <summary>
//   The user authentication.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Sample7_Service
{
    using System;
    using System.Diagnostics;
    using System.IdentityModel.Selectors;

    /// <summary>
    /// The user authentication.
    /// </summary>
    public class UserAuthentication : UserNamePasswordValidator
    {
        /// <summary>
        /// The validate.
        /// </summary>
        /// <param name="userName">
        /// The user name.
        /// </param>
        /// <param name="password">
        /// The password.
        /// </param>
        /// <exception cref="FaultException">
        /// </exception>
        public override void Validate(string userName, string password)
        {
            Debug.WriteLine("Validating user = {0}, pass = {1}", userName, password);
            if (userName == password)
            {
                return;
            }

            throw new AccessViolationException("YOU SHELL NOT PASS");
        }
    }
}
