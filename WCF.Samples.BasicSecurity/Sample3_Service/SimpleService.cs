﻿using SimpleService.Contracts;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Sample3_Service
{
    public class SimpleService : ISimpleService
    {
        public string Method(string param1)
        {
            Debug.Print(OperationContext.Current.EndpointDispatcher.EndpointAddress.Uri.ToString());

            var securityContext = OperationContext.Current.ServiceSecurityContext;

            if (securityContext == null)
                Debug.Print("No SecurityContext");
            else if (securityContext.IsAnonymous)
                Debug.Print("Anonymouse");
            else
            {
                Debug.Print(securityContext.PrimaryIdentity.Name);
                if (securityContext.WindowsIdentity != null)
                {
                    Debug.Print(securityContext.WindowsIdentity.User.AccountDomainSid.ToString());
                }
            }
            
            return "Result";
        }
    }
}
