﻿namespace Sample7_WebHost
{
    using System;
    using System.Security.Permissions;

    using SimpleService.Contracts;

    /// <summary>
    /// The calculator service.
    /// </summary>
    public class CalculatorService : ICalculatorService
    {
        /// <summary>
        /// The pi.
        /// </summary>
        /// <returns>
        /// The <see cref="double"/>.
        /// </returns>
        [PrincipalPermission(SecurityAction.Demand, Role = "admin")]
        public double PI()
        {
            return Math.PI;
        }

        /// <summary>
        /// The add.
        /// </summary>
        /// <param name="a">
        /// The a.
        /// </param>
        /// <param name="b">
        /// The b.
        /// </param>
        /// <returns>
        /// The <see cref="double"/>.
        /// </returns>
        [PrincipalPermission(SecurityAction.Demand, Role = "admin")]
        [PrincipalPermission(SecurityAction.Demand, Role = "user")]
        public double Add(double a, double b)
        {
            return a + b;
        }
    }
}
