﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomRoleProvider.cs" company="">
//   
// </copyright>
// <summary>
//   The custom role provider.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Sample7_WebHost
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Security;

    /// <summary>
    /// The custom role provider.
    /// </summary>
    class CustomRoleProvider: RoleProvider
    {
        /// <summary>
        /// The in memory roles.
        /// </summary>
        private readonly IList<string> InMemoryRoles;

        /// <summary>
        /// The in memory user roles.
        /// </summary>
        private readonly IDictionary<string, IEnumerable<string>> InMemoryUserRoles;

        /// <summary>
        /// Initializes a new instance of the <see cref="CustomRoleProvider"/> class.
        /// </summary>
        public CustomRoleProvider()
        {
            this.InMemoryRoles = new List<string>
                                     {
                                         "admin",
                                         "user",
                                         "anonymous"
                                     };
            this.InMemoryUserRoles = new Dictionary<string, IEnumerable<string>>
                                         {
                                             { "admin", this.InMemoryRoles },
                                             { "user", this.InMemoryRoles.Skip(1) }
                                         };
        }

        public override bool IsUserInRole(string username, string roleName)
        {
            return this.InMemoryUserRoles.ContainsKey(username.ToLower()) && this.InMemoryUserRoles[username.ToLower()].Contains(roleName.ToLower());
        }

        public override string[] GetRolesForUser(string username)
        {
            throw new NotImplementedException();
        }

        public override void CreateRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            throw new NotImplementedException();
        }

        public override bool RoleExists(string roleName)
        {
            throw new NotImplementedException();
        }

        public override void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override string[] GetUsersInRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override string[] GetAllRoles()
        {
            throw new NotImplementedException();
        }

        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            throw new NotImplementedException();
        }

        public override string ApplicationName { get; set; }
    }
}
