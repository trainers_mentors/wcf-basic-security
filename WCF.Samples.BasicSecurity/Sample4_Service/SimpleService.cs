﻿using SimpleService.Contracts;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Sample4_Service
{
    public class SimpleService : ISimpleService
    {
        public string Method(string param1)
        {
            var returnBuffer = new StringBuilder();

            returnBuffer.AppendLine(OperationContext.Current.EndpointDispatcher.EndpointAddress.Uri.ToString());
            var securityContext = OperationContext.Current.ServiceSecurityContext;

            returnBuffer.AppendFormat("Authentication type: {0}\n", securityContext.PrimaryIdentity.AuthenticationType);
            returnBuffer.AppendFormat("Name: {0}\n", securityContext.PrimaryIdentity.Name);

            return returnBuffer.ToString();
        }
    }
}
