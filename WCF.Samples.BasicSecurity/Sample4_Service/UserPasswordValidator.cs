﻿namespace Sample4_Service
{
    using System.IdentityModel.Selectors;
    using System.IdentityModel.Tokens;

    /// <summary>
    /// The user password validator.
    /// </summary>
    public class UserPasswordValidator : UserNamePasswordValidator
    {
        /// <summary>
        /// The validate.
        /// </summary>
        /// <param name="userName">
        /// The user name.
        /// </param>
        /// <param name="password">
        /// The password.
        /// </param>
        /// <exception cref="SecurityTokenValidationException">
        /// </exception>
        public override void Validate(string userName, string password)
        {
            if (userName == "user" && password == "pass")
                return;

            throw new SecurityTokenValidationException();
        }

    }
}
