﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace Sample2
{
    class Program
    {
        static void Main(string[] args)
        {
            AppDomain.CurrentDomain.SetPrincipalPolicy(PrincipalPolicy.WindowsPrincipal);

            var principal = System.Threading.Thread.CurrentPrincipal;
            var identity = principal.Identity;

            Console.WriteLine("Name {0}", identity.Name);
            Console.WriteLine("Authentication type {0}", identity.AuthenticationType);
            Console.WriteLine("Is Administrators: {0}", principal.IsInRole("BUILTIN\\Administrators"));
            Console.WriteLine("Is User: {0}", principal.IsInRole("BUILTIN\\Users"));
                        
            var windowsIdentity = identity as WindowsIdentity;
            Console.WriteLine("SID: {0}", windowsIdentity.User.AccountDomainSid.ToString());
            
            Console.ReadLine();
        }
    }
}
