﻿namespace SimpleService.Contracts
{
    using System.ServiceModel;

    [ServiceContract]
    public interface ICalculatorService
    {
        [OperationContract]
        double PI();


        [OperationContract]
        double Add(double a, double b);
    }
}
