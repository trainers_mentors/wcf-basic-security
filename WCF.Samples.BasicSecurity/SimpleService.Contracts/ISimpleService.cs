﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace SimpleService.Contracts
{
    [ServiceContract]
    public interface ISimpleService
    {
        [OperationContract]
        string Method(string param1);
    }
}
