﻿using SimpleService.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Sample5_Client
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                var channelFactory = new ChannelFactory<ISimpleService>("Windows");
                channelFactory.Credentials.Windows.AllowedImpersonationLevel = TokenImpersonationLevel.Impersonation;
                var channel = channelFactory.CreateChannel();

                Console.WriteLine(channel.Method(""));
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            Console.ReadLine();
        }
    }
}
